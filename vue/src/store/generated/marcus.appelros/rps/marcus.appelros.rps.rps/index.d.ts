import { Match } from "./module/types/rps/match";
import { Params } from "./module/types/rps/params";
import { Player } from "./module/types/rps/player";
import { Queue } from "./module/types/rps/queue";
export { Match, Params, Player, Queue };
declare const _default;
export default _default;

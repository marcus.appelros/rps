import { Reader, Writer } from "protobufjs/minimal";
import { Params } from "../rps/params";
import { Queue } from "../rps/queue";
import { Match } from "../rps/match";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { Player } from "../rps/player";
export declare const protobufPackage = "marcus.appelros.rps.rps";
/** QueryParamsRequest is request type for the Query/Params RPC method. */
export interface QueryParamsRequest {
}
/** QueryParamsResponse is response type for the Query/Params RPC method. */
export interface QueryParamsResponse {
    /** params holds all the parameters of this module. */
    params: Params | undefined;
}
export interface QueryGetQueueRequest {
}
export interface QueryGetQueueResponse {
    Queue: Queue | undefined;
}
export interface QueryGetMatchRequest {
    id: number;
}
export interface QueryGetMatchResponse {
    Match: Match | undefined;
}
export interface QueryAllMatchRequest {
    pagination: PageRequest | undefined;
}
export interface QueryAllMatchResponse {
    Match: Match[];
    pagination: PageResponse | undefined;
}
export interface QueryGetPlayerRequest {
    index: string;
}
export interface QueryGetPlayerResponse {
    player: Player | undefined;
}
export interface QueryAllPlayerRequest {
    pagination: PageRequest | undefined;
}
export interface QueryAllPlayerResponse {
    player: Player[];
    pagination: PageResponse | undefined;
}
export declare const QueryParamsRequest: {
    encode(_: QueryParamsRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsRequest;
    fromJSON(_: any): QueryParamsRequest;
    toJSON(_: QueryParamsRequest): unknown;
    fromPartial(_: DeepPartial<QueryParamsRequest>): QueryParamsRequest;
};
export declare const QueryParamsResponse: {
    encode(message: QueryParamsResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsResponse;
    fromJSON(object: any): QueryParamsResponse;
    toJSON(message: QueryParamsResponse): unknown;
    fromPartial(object: DeepPartial<QueryParamsResponse>): QueryParamsResponse;
};
export declare const QueryGetQueueRequest: {
    encode(_: QueryGetQueueRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetQueueRequest;
    fromJSON(_: any): QueryGetQueueRequest;
    toJSON(_: QueryGetQueueRequest): unknown;
    fromPartial(_: DeepPartial<QueryGetQueueRequest>): QueryGetQueueRequest;
};
export declare const QueryGetQueueResponse: {
    encode(message: QueryGetQueueResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetQueueResponse;
    fromJSON(object: any): QueryGetQueueResponse;
    toJSON(message: QueryGetQueueResponse): unknown;
    fromPartial(object: DeepPartial<QueryGetQueueResponse>): QueryGetQueueResponse;
};
export declare const QueryGetMatchRequest: {
    encode(message: QueryGetMatchRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetMatchRequest;
    fromJSON(object: any): QueryGetMatchRequest;
    toJSON(message: QueryGetMatchRequest): unknown;
    fromPartial(object: DeepPartial<QueryGetMatchRequest>): QueryGetMatchRequest;
};
export declare const QueryGetMatchResponse: {
    encode(message: QueryGetMatchResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetMatchResponse;
    fromJSON(object: any): QueryGetMatchResponse;
    toJSON(message: QueryGetMatchResponse): unknown;
    fromPartial(object: DeepPartial<QueryGetMatchResponse>): QueryGetMatchResponse;
};
export declare const QueryAllMatchRequest: {
    encode(message: QueryAllMatchRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllMatchRequest;
    fromJSON(object: any): QueryAllMatchRequest;
    toJSON(message: QueryAllMatchRequest): unknown;
    fromPartial(object: DeepPartial<QueryAllMatchRequest>): QueryAllMatchRequest;
};
export declare const QueryAllMatchResponse: {
    encode(message: QueryAllMatchResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllMatchResponse;
    fromJSON(object: any): QueryAllMatchResponse;
    toJSON(message: QueryAllMatchResponse): unknown;
    fromPartial(object: DeepPartial<QueryAllMatchResponse>): QueryAllMatchResponse;
};
export declare const QueryGetPlayerRequest: {
    encode(message: QueryGetPlayerRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetPlayerRequest;
    fromJSON(object: any): QueryGetPlayerRequest;
    toJSON(message: QueryGetPlayerRequest): unknown;
    fromPartial(object: DeepPartial<QueryGetPlayerRequest>): QueryGetPlayerRequest;
};
export declare const QueryGetPlayerResponse: {
    encode(message: QueryGetPlayerResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryGetPlayerResponse;
    fromJSON(object: any): QueryGetPlayerResponse;
    toJSON(message: QueryGetPlayerResponse): unknown;
    fromPartial(object: DeepPartial<QueryGetPlayerResponse>): QueryGetPlayerResponse;
};
export declare const QueryAllPlayerRequest: {
    encode(message: QueryAllPlayerRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllPlayerRequest;
    fromJSON(object: any): QueryAllPlayerRequest;
    toJSON(message: QueryAllPlayerRequest): unknown;
    fromPartial(object: DeepPartial<QueryAllPlayerRequest>): QueryAllPlayerRequest;
};
export declare const QueryAllPlayerResponse: {
    encode(message: QueryAllPlayerResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAllPlayerResponse;
    fromJSON(object: any): QueryAllPlayerResponse;
    toJSON(message: QueryAllPlayerResponse): unknown;
    fromPartial(object: DeepPartial<QueryAllPlayerResponse>): QueryAllPlayerResponse;
};
/** Query defines the gRPC querier service. */
export interface Query {
    /** Parameters queries the parameters of the module. */
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    /** Queries a Queue by index. */
    Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse>;
    /** Queries a Match by id. */
    Match(request: QueryGetMatchRequest): Promise<QueryGetMatchResponse>;
    /** Queries a list of Match items. */
    MatchAll(request: QueryAllMatchRequest): Promise<QueryAllMatchResponse>;
    /** Queries a Player by index. */
    Player(request: QueryGetPlayerRequest): Promise<QueryGetPlayerResponse>;
    /** Queries a list of Player items. */
    PlayerAll(request: QueryAllPlayerRequest): Promise<QueryAllPlayerResponse>;
}
export declare class QueryClientImpl implements Query {
    private readonly rpc;
    constructor(rpc: Rpc);
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    Queue(request: QueryGetQueueRequest): Promise<QueryGetQueueResponse>;
    Match(request: QueryGetMatchRequest): Promise<QueryGetMatchResponse>;
    MatchAll(request: QueryAllMatchRequest): Promise<QueryAllMatchResponse>;
    Player(request: QueryGetPlayerRequest): Promise<QueryGetPlayerResponse>;
    PlayerAll(request: QueryAllPlayerRequest): Promise<QueryAllPlayerResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};

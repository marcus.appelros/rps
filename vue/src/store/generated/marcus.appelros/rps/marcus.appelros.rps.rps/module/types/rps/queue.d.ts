import { Writer, Reader } from "protobufjs/minimal";
export declare const protobufPackage = "marcus.appelros.rps.rps";
export interface Queue {
    address: string;
}
export declare const Queue: {
    encode(message: Queue, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Queue;
    fromJSON(object: any): Queue;
    toJSON(message: Queue): unknown;
    fromPartial(object: DeepPartial<Queue>): Queue;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};

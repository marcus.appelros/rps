/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";
export const protobufPackage = "marcus.appelros.rps.rps";
const baseMatch = {
    id: 0,
    player1: "",
    player2: "",
    player1Move: 0,
    player2Move: 0,
    winner: 0,
};
export const Match = {
    encode(message, writer = Writer.create()) {
        if (message.id !== 0) {
            writer.uint32(8).uint64(message.id);
        }
        if (message.player1 !== "") {
            writer.uint32(18).string(message.player1);
        }
        if (message.player2 !== "") {
            writer.uint32(26).string(message.player2);
        }
        if (message.player1Move !== 0) {
            writer.uint32(32).uint64(message.player1Move);
        }
        if (message.player2Move !== 0) {
            writer.uint32(40).uint64(message.player2Move);
        }
        if (message.winner !== 0) {
            writer.uint32(48).uint64(message.winner);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMatch };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = longToNumber(reader.uint64());
                    break;
                case 2:
                    message.player1 = reader.string();
                    break;
                case 3:
                    message.player2 = reader.string();
                    break;
                case 4:
                    message.player1Move = longToNumber(reader.uint64());
                    break;
                case 5:
                    message.player2Move = longToNumber(reader.uint64());
                    break;
                case 6:
                    message.winner = longToNumber(reader.uint64());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMatch };
        if (object.id !== undefined && object.id !== null) {
            message.id = Number(object.id);
        }
        else {
            message.id = 0;
        }
        if (object.player1 !== undefined && object.player1 !== null) {
            message.player1 = String(object.player1);
        }
        else {
            message.player1 = "";
        }
        if (object.player2 !== undefined && object.player2 !== null) {
            message.player2 = String(object.player2);
        }
        else {
            message.player2 = "";
        }
        if (object.player1Move !== undefined && object.player1Move !== null) {
            message.player1Move = Number(object.player1Move);
        }
        else {
            message.player1Move = 0;
        }
        if (object.player2Move !== undefined && object.player2Move !== null) {
            message.player2Move = Number(object.player2Move);
        }
        else {
            message.player2Move = 0;
        }
        if (object.winner !== undefined && object.winner !== null) {
            message.winner = Number(object.winner);
        }
        else {
            message.winner = 0;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.player1 !== undefined && (obj.player1 = message.player1);
        message.player2 !== undefined && (obj.player2 = message.player2);
        message.player1Move !== undefined &&
            (obj.player1Move = message.player1Move);
        message.player2Move !== undefined &&
            (obj.player2Move = message.player2Move);
        message.winner !== undefined && (obj.winner = message.winner);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMatch };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = 0;
        }
        if (object.player1 !== undefined && object.player1 !== null) {
            message.player1 = object.player1;
        }
        else {
            message.player1 = "";
        }
        if (object.player2 !== undefined && object.player2 !== null) {
            message.player2 = object.player2;
        }
        else {
            message.player2 = "";
        }
        if (object.player1Move !== undefined && object.player1Move !== null) {
            message.player1Move = object.player1Move;
        }
        else {
            message.player1Move = 0;
        }
        if (object.player2Move !== undefined && object.player2Move !== null) {
            message.player2Move = object.player2Move;
        }
        else {
            message.player2Move = 0;
        }
        if (object.winner !== undefined && object.winner !== null) {
            message.winner = object.winner;
        }
        else {
            message.winner = 0;
        }
        return message;
    },
};
var globalThis = (() => {
    if (typeof globalThis !== "undefined")
        return globalThis;
    if (typeof self !== "undefined")
        return self;
    if (typeof window !== "undefined")
        return window;
    if (typeof global !== "undefined")
        return global;
    throw "Unable to locate global object";
})();
function longToNumber(long) {
    if (long.gt(Number.MAX_SAFE_INTEGER)) {
        throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
    }
    return long.toNumber();
}
if (util.Long !== Long) {
    util.Long = Long;
    configure();
}

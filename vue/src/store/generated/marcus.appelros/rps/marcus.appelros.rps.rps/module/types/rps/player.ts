/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";

export const protobufPackage = "marcus.appelros.rps.rps";

export interface Player {
  index: string;
  ongoing: number[];
  completed: number[];
}

const basePlayer: object = { index: "", ongoing: 0, completed: 0 };

export const Player = {
  encode(message: Player, writer: Writer = Writer.create()): Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    writer.uint32(18).fork();
    for (const v of message.ongoing) {
      writer.uint64(v);
    }
    writer.ldelim();
    writer.uint32(26).fork();
    for (const v of message.completed) {
      writer.uint64(v);
    }
    writer.ldelim();
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Player {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePlayer } as Player;
    message.ongoing = [];
    message.completed = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        case 2:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.ongoing.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.ongoing.push(longToNumber(reader.uint64() as Long));
          }
          break;
        case 3:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.completed.push(longToNumber(reader.uint64() as Long));
            }
          } else {
            message.completed.push(longToNumber(reader.uint64() as Long));
          }
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Player {
    const message = { ...basePlayer } as Player;
    message.ongoing = [];
    message.completed = [];
    if (object.index !== undefined && object.index !== null) {
      message.index = String(object.index);
    } else {
      message.index = "";
    }
    if (object.ongoing !== undefined && object.ongoing !== null) {
      for (const e of object.ongoing) {
        message.ongoing.push(Number(e));
      }
    }
    if (object.completed !== undefined && object.completed !== null) {
      for (const e of object.completed) {
        message.completed.push(Number(e));
      }
    }
    return message;
  },

  toJSON(message: Player): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    if (message.ongoing) {
      obj.ongoing = message.ongoing.map((e) => e);
    } else {
      obj.ongoing = [];
    }
    if (message.completed) {
      obj.completed = message.completed.map((e) => e);
    } else {
      obj.completed = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<Player>): Player {
    const message = { ...basePlayer } as Player;
    message.ongoing = [];
    message.completed = [];
    if (object.index !== undefined && object.index !== null) {
      message.index = object.index;
    } else {
      message.index = "";
    }
    if (object.ongoing !== undefined && object.ongoing !== null) {
      for (const e of object.ongoing) {
        message.ongoing.push(e);
      }
    }
    if (object.completed !== undefined && object.completed !== null) {
      for (const e of object.completed) {
        message.completed.push(e);
      }
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}

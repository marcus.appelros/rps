/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";
import { Params } from "../rps/params";
import { Queue } from "../rps/queue";
import { Match } from "../rps/match";
import { Player } from "../rps/player";

export const protobufPackage = "marcus.appelros.rps.rps";

/** GenesisState defines the rps module's genesis state. */
export interface GenesisState {
  params: Params | undefined;
  queue: Queue | undefined;
  matchList: Match[];
  matchCount: number;
  /** this line is used by starport scaffolding # genesis/proto/state */
  playerList: Player[];
}

const baseGenesisState: object = { matchCount: 0 };

export const GenesisState = {
  encode(message: GenesisState, writer: Writer = Writer.create()): Writer {
    if (message.params !== undefined) {
      Params.encode(message.params, writer.uint32(10).fork()).ldelim();
    }
    if (message.queue !== undefined) {
      Queue.encode(message.queue, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.matchList) {
      Match.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.matchCount !== 0) {
      writer.uint32(32).uint64(message.matchCount);
    }
    for (const v of message.playerList) {
      Player.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGenesisState } as GenesisState;
    message.matchList = [];
    message.playerList = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.params = Params.decode(reader, reader.uint32());
          break;
        case 2:
          message.queue = Queue.decode(reader, reader.uint32());
          break;
        case 3:
          message.matchList.push(Match.decode(reader, reader.uint32()));
          break;
        case 4:
          message.matchCount = longToNumber(reader.uint64() as Long);
          break;
        case 5:
          message.playerList.push(Player.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.matchList = [];
    message.playerList = [];
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromJSON(object.params);
    } else {
      message.params = undefined;
    }
    if (object.queue !== undefined && object.queue !== null) {
      message.queue = Queue.fromJSON(object.queue);
    } else {
      message.queue = undefined;
    }
    if (object.matchList !== undefined && object.matchList !== null) {
      for (const e of object.matchList) {
        message.matchList.push(Match.fromJSON(e));
      }
    }
    if (object.matchCount !== undefined && object.matchCount !== null) {
      message.matchCount = Number(object.matchCount);
    } else {
      message.matchCount = 0;
    }
    if (object.playerList !== undefined && object.playerList !== null) {
      for (const e of object.playerList) {
        message.playerList.push(Player.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    message.params !== undefined &&
      (obj.params = message.params ? Params.toJSON(message.params) : undefined);
    message.queue !== undefined &&
      (obj.queue = message.queue ? Queue.toJSON(message.queue) : undefined);
    if (message.matchList) {
      obj.matchList = message.matchList.map((e) =>
        e ? Match.toJSON(e) : undefined
      );
    } else {
      obj.matchList = [];
    }
    message.matchCount !== undefined && (obj.matchCount = message.matchCount);
    if (message.playerList) {
      obj.playerList = message.playerList.map((e) =>
        e ? Player.toJSON(e) : undefined
      );
    } else {
      obj.playerList = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<GenesisState>): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.matchList = [];
    message.playerList = [];
    if (object.params !== undefined && object.params !== null) {
      message.params = Params.fromPartial(object.params);
    } else {
      message.params = undefined;
    }
    if (object.queue !== undefined && object.queue !== null) {
      message.queue = Queue.fromPartial(object.queue);
    } else {
      message.queue = undefined;
    }
    if (object.matchList !== undefined && object.matchList !== null) {
      for (const e of object.matchList) {
        message.matchList.push(Match.fromPartial(e));
      }
    }
    if (object.matchCount !== undefined && object.matchCount !== null) {
      message.matchCount = object.matchCount;
    } else {
      message.matchCount = 0;
    }
    if (object.playerList !== undefined && object.playerList !== null) {
      for (const e of object.playerList) {
        message.playerList.push(Player.fromPartial(e));
      }
    }
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}

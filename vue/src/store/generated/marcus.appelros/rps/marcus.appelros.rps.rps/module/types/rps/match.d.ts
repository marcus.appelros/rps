import { Writer, Reader } from "protobufjs/minimal";
export declare const protobufPackage = "marcus.appelros.rps.rps";
export interface Match {
    id: number;
    player1: string;
    player2: string;
    player1Move: number;
    player2Move: number;
    winner: number;
}
export declare const Match: {
    encode(message: Match, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Match;
    fromJSON(object: any): Match;
    toJSON(message: Match): unknown;
    fromPartial(object: DeepPartial<Match>): Match;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};

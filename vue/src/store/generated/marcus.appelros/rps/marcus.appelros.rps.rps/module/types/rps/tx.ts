/* eslint-disable */
import { Reader, util, configure, Writer } from "protobufjs/minimal";
import * as Long from "long";

export const protobufPackage = "marcus.appelros.rps.rps";

export interface MsgJoinQueue {
  creator: string;
}

export interface MsgJoinQueueResponse {}

export interface MsgSubmitMove {
  creator: string;
  matchId: number;
  moveId: number;
}

export interface MsgSubmitMoveResponse {}

const baseMsgJoinQueue: object = { creator: "" };

export const MsgJoinQueue = {
  encode(message: MsgJoinQueue, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgJoinQueue {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgJoinQueue {
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    return message;
  },

  toJSON(message: MsgJoinQueue): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgJoinQueue>): MsgJoinQueue {
    const message = { ...baseMsgJoinQueue } as MsgJoinQueue;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    return message;
  },
};

const baseMsgJoinQueueResponse: object = {};

export const MsgJoinQueueResponse = {
  encode(_: MsgJoinQueueResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgJoinQueueResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgJoinQueueResponse {
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    return message;
  },

  toJSON(_: MsgJoinQueueResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgJoinQueueResponse>): MsgJoinQueueResponse {
    const message = { ...baseMsgJoinQueueResponse } as MsgJoinQueueResponse;
    return message;
  },
};

const baseMsgSubmitMove: object = { creator: "", matchId: 0, moveId: 0 };

export const MsgSubmitMove = {
  encode(message: MsgSubmitMove, writer: Writer = Writer.create()): Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.matchId !== 0) {
      writer.uint32(16).uint64(message.matchId);
    }
    if (message.moveId !== 0) {
      writer.uint32(24).uint64(message.moveId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgSubmitMove {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.matchId = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.moveId = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgSubmitMove {
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator);
    } else {
      message.creator = "";
    }
    if (object.matchId !== undefined && object.matchId !== null) {
      message.matchId = Number(object.matchId);
    } else {
      message.matchId = 0;
    }
    if (object.moveId !== undefined && object.moveId !== null) {
      message.moveId = Number(object.moveId);
    } else {
      message.moveId = 0;
    }
    return message;
  },

  toJSON(message: MsgSubmitMove): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.matchId !== undefined && (obj.matchId = message.matchId);
    message.moveId !== undefined && (obj.moveId = message.moveId);
    return obj;
  },

  fromPartial(object: DeepPartial<MsgSubmitMove>): MsgSubmitMove {
    const message = { ...baseMsgSubmitMove } as MsgSubmitMove;
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator;
    } else {
      message.creator = "";
    }
    if (object.matchId !== undefined && object.matchId !== null) {
      message.matchId = object.matchId;
    } else {
      message.matchId = 0;
    }
    if (object.moveId !== undefined && object.moveId !== null) {
      message.moveId = object.moveId;
    } else {
      message.moveId = 0;
    }
    return message;
  },
};

const baseMsgSubmitMoveResponse: object = {};

export const MsgSubmitMoveResponse = {
  encode(_: MsgSubmitMoveResponse, writer: Writer = Writer.create()): Writer {
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgSubmitMoveResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgSubmitMoveResponse {
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    return message;
  },

  toJSON(_: MsgSubmitMoveResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(_: DeepPartial<MsgSubmitMoveResponse>): MsgSubmitMoveResponse {
    const message = { ...baseMsgSubmitMoveResponse } as MsgSubmitMoveResponse;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse>;
  /** this line is used by starport scaffolding # proto/tx/rpc */
  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse> {
    const data = MsgJoinQueue.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.rps.rps.Msg",
      "JoinQueue",
      data
    );
    return promise.then((data) =>
      MsgJoinQueueResponse.decode(new Reader(data))
    );
  }

  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse> {
    const data = MsgSubmitMove.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.rps.rps.Msg",
      "SubmitMove",
      data
    );
    return promise.then((data) =>
      MsgSubmitMoveResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}

/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "marcus.appelros.rps.rps";

export interface MsgJoinQueue {
  creator: string;
}

export interface MsgJoinQueueResponse {}

export interface MsgSubmitMove {
  creator: string;
  matchId: Long;
  moveId: Long;
}

export interface MsgSubmitMoveResponse {}

function createBaseMsgJoinQueue(): MsgJoinQueue {
  return { creator: "" };
}

export const MsgJoinQueue = {
  encode(
    message: MsgJoinQueue,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgJoinQueue {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgJoinQueue();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgJoinQueue {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
    };
  },

  toJSON(message: MsgJoinQueue): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgJoinQueue>, I>>(
    object: I
  ): MsgJoinQueue {
    const message = createBaseMsgJoinQueue();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgJoinQueueResponse(): MsgJoinQueueResponse {
  return {};
}

export const MsgJoinQueueResponse = {
  encode(
    _: MsgJoinQueueResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): MsgJoinQueueResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgJoinQueueResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgJoinQueueResponse {
    return {};
  },

  toJSON(_: MsgJoinQueueResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgJoinQueueResponse>, I>>(
    _: I
  ): MsgJoinQueueResponse {
    const message = createBaseMsgJoinQueueResponse();
    return message;
  },
};

function createBaseMsgSubmitMove(): MsgSubmitMove {
  return { creator: "", matchId: Long.UZERO, moveId: Long.UZERO };
}

export const MsgSubmitMove = {
  encode(
    message: MsgSubmitMove,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (!message.matchId.isZero()) {
      writer.uint32(16).uint64(message.matchId);
    }
    if (!message.moveId.isZero()) {
      writer.uint32(24).uint64(message.moveId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgSubmitMove {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgSubmitMove();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.matchId = reader.uint64() as Long;
          break;
        case 3:
          message.moveId = reader.uint64() as Long;
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgSubmitMove {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      matchId: isSet(object.matchId)
        ? Long.fromString(object.matchId)
        : Long.UZERO,
      moveId: isSet(object.moveId)
        ? Long.fromString(object.moveId)
        : Long.UZERO,
    };
  },

  toJSON(message: MsgSubmitMove): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.matchId !== undefined &&
      (obj.matchId = (message.matchId || Long.UZERO).toString());
    message.moveId !== undefined &&
      (obj.moveId = (message.moveId || Long.UZERO).toString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgSubmitMove>, I>>(
    object: I
  ): MsgSubmitMove {
    const message = createBaseMsgSubmitMove();
    message.creator = object.creator ?? "";
    message.matchId =
      object.matchId !== undefined && object.matchId !== null
        ? Long.fromValue(object.matchId)
        : Long.UZERO;
    message.moveId =
      object.moveId !== undefined && object.moveId !== null
        ? Long.fromValue(object.moveId)
        : Long.UZERO;
    return message;
  },
};

function createBaseMsgSubmitMoveResponse(): MsgSubmitMoveResponse {
  return {};
}

export const MsgSubmitMoveResponse = {
  encode(
    _: MsgSubmitMoveResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): MsgSubmitMoveResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgSubmitMoveResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgSubmitMoveResponse {
    return {};
  },

  toJSON(_: MsgSubmitMoveResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgSubmitMoveResponse>, I>>(
    _: I
  ): MsgSubmitMoveResponse {
    const message = createBaseMsgSubmitMoveResponse();
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse>;
  /** this line is used by starport scaffolding # proto/tx/rpc */
  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.JoinQueue = this.JoinQueue.bind(this);
    this.SubmitMove = this.SubmitMove.bind(this);
  }
  JoinQueue(request: MsgJoinQueue): Promise<MsgJoinQueueResponse> {
    const data = MsgJoinQueue.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.rps.rps.Msg",
      "JoinQueue",
      data
    );
    return promise.then((data) =>
      MsgJoinQueueResponse.decode(new _m0.Reader(data))
    );
  }

  SubmitMove(request: MsgSubmitMove): Promise<MsgSubmitMoveResponse> {
    const data = MsgSubmitMove.encode(request).finish();
    const promise = this.rpc.request(
      "marcus.appelros.rps.rps.Msg",
      "SubmitMove",
      data
    );
    return promise.then((data) =>
      MsgSubmitMoveResponse.decode(new _m0.Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Long
  ? string | number | Long
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

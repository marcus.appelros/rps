import { Registry } from "@cosmjs/proto-signing";
import {
	defaultRegistryTypes as defaultStargateTypes,
	SigningStargateClient,
} from "@cosmjs/stargate";
import { MsgJoinQueue, MsgSubmitMove } from "./tx.js";

var rps = {};
window.rps = rps;

const customRegistry = new Registry(defaultStargateTypes);
customRegistry.register("/marcus.appelros.rps.rps.MsgJoinQueue", MsgJoinQueue);
customRegistry.register("/marcus.appelros.rps.rps.MsgSubmitMove", MsgSubmitMove);

async function getClient() {
	await window.keplr.enable("rps");
	const offlineSigner = window.keplr.getOfflineSigner("rps");
	const client = await SigningStargateClient.connectWithSigner(
		"http://0.0.0.0:26657",
		offlineSigner,
		{ registry: customRegistry },
	);
	return client;
}
rps.getClient = getClient;

async function joinQueue() {
	const client = await getClient();
	const accounts = await client.signer.getAccounts();
	const message = {
		typeUrl: "/marcus.appelros.rps.rps.MsgJoinQueue",
		value: MsgJoinQueue.fromPartial({
			creator: accounts[0].address,
		}),
	};
	const fee = {
		amount: [
			{
				denom: "stake",
				amount: "100",
			},
		],
		gas: "100000",
	};
	const response = await client.signAndBroadcast(accounts[0].address, [message], fee);
	return response;
}
rps.joinQueue = joinQueue;

async function submitMove(matchId, moveId) {
	const client = await getClient();
	const accounts = await client.signer.getAccounts();
	const message = {
		typeUrl: "/marcus.appelros.rps.rps.MsgSubmitMove",
		value: MsgSubmitMove.fromPartial({
			creator: accounts[0].address,
			matchId: matchId,
			moveId: moveId,
		}),
	};
	const fee = {
		amount: [
			{
				denom: "stake",
				amount: "100",
			},
		],
		gas: "100000",
	};
	const response = await client.signAndBroadcast(accounts[0].address, [message], fee);
	return response;
}
rps.submitMove = submitMove;

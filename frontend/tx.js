"use strict";
exports.__esModule = true;
exports.MsgClientImpl = exports.MsgSubmitMoveResponse = exports.MsgSubmitMove = exports.MsgJoinQueueResponse = exports.MsgJoinQueue = exports.protobufPackage = void 0;
/* eslint-disable */
var long_1 = require("long");
var minimal_1 = require("protobufjs/minimal");
exports.protobufPackage = "marcus.appelros.rps.rps";
function createBaseMsgJoinQueue() {
    return { creator: "" };
}
exports.MsgJoinQueue = {
    encode: function (message, writer) {
        if (writer === void 0) { writer = minimal_1.Writer.create(); }
        if (message.creator !== "") {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1.Reader ? input : new minimal_1.Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = createBaseMsgJoinQueue();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (object) {
        return {
            creator: isSet(object.creator) ? String(object.creator) : ""
        };
    },
    toJSON: function (message) {
        var obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial: function (object) {
        var _a;
        var message = createBaseMsgJoinQueue();
        message.creator = (_a = object.creator) !== null && _a !== void 0 ? _a : "";
        return message;
    }
};
function createBaseMsgJoinQueueResponse() {
    return {};
}
exports.MsgJoinQueueResponse = {
    encode: function (_, writer) {
        if (writer === void 0) { writer = minimal_1.Writer.create(); }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1.Reader ? input : new minimal_1.Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = createBaseMsgJoinQueueResponse();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (_) {
        return {};
    },
    toJSON: function (_) {
        var obj = {};
        return obj;
    },
    fromPartial: function (_) {
        var message = createBaseMsgJoinQueueResponse();
        return message;
    }
};
function createBaseMsgSubmitMove() {
    return { creator: "", matchId: long_1.UZERO, moveId: long_1.UZERO };
}
exports.MsgSubmitMove = {
    encode: function (message, writer) {
        if (writer === void 0) { writer = minimal_1.Writer.create(); }
        if (message.creator !== "") {
            writer.uint32(10).string(message.creator);
        }
        if (!message.matchId.isZero()) {
            writer.uint32(16).uint64(message.matchId);
        }
        if (!message.moveId.isZero()) {
            writer.uint32(24).uint64(message.moveId);
        }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1.Reader ? input : new minimal_1.Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = createBaseMsgSubmitMove();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.matchId = reader.uint64();
                    break;
                case 3:
                    message.moveId = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (object) {
        return {
            creator: isSet(object.creator) ? String(object.creator) : "",
            matchId: isSet(object.matchId)
                ? long_1.fromString(object.matchId)
                : long_1.UZERO,
            moveId: isSet(object.moveId)
                ? long_1.fromString(object.moveId)
                : long_1.UZERO
        };
    },
    toJSON: function (message) {
        var obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.matchId !== undefined &&
            (obj.matchId = (message.matchId || long_1.UZERO).toString());
        message.moveId !== undefined &&
            (obj.moveId = (message.moveId || long_1.UZERO).toString());
        return obj;
    },
    fromPartial: function (object) {
        var _a;
        var message = createBaseMsgSubmitMove();
        message.creator = (_a = object.creator) !== null && _a !== void 0 ? _a : "";
        message.matchId =
            object.matchId !== undefined && object.matchId !== null
                ? long_1.fromValue(object.matchId)
                : long_1.UZERO;
        message.moveId =
            object.moveId !== undefined && object.moveId !== null
                ? long_1.fromValue(object.moveId)
                : long_1.UZERO;
        return message;
    }
};
function createBaseMsgSubmitMoveResponse() {
    return {};
}
exports.MsgSubmitMoveResponse = {
    encode: function (_, writer) {
        if (writer === void 0) { writer = minimal_1.Writer.create(); }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1.Reader ? input : new minimal_1.Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = createBaseMsgSubmitMoveResponse();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (_) {
        return {};
    },
    toJSON: function (_) {
        var obj = {};
        return obj;
    },
    fromPartial: function (_) {
        var message = createBaseMsgSubmitMoveResponse();
        return message;
    }
};
var MsgClientImpl = /** @class */ (function () {
    function MsgClientImpl(rpc) {
        this.rpc = rpc;
        this.JoinQueue = this.JoinQueue.bind(this);
        this.SubmitMove = this.SubmitMove.bind(this);
    }
    MsgClientImpl.prototype.JoinQueue = function (request) {
        var data = exports.MsgJoinQueue.encode(request).finish();
        var promise = this.rpc.request("marcus.appelros.rps.rps.Msg", "JoinQueue", data);
        return promise.then(function (data) {
            return exports.MsgJoinQueueResponse.decode(new minimal_1.Reader(data));
        });
    };
    MsgClientImpl.prototype.SubmitMove = function (request) {
        var data = exports.MsgSubmitMove.encode(request).finish();
        var promise = this.rpc.request("marcus.appelros.rps.rps.Msg", "SubmitMove", data);
        return promise.then(function (data) {
            return exports.MsgSubmitMoveResponse.decode(new minimal_1.Reader(data));
        });
    };
    return MsgClientImpl;
}());
exports.MsgClientImpl = MsgClientImpl;
if (minimal_1.util.Long !== long_1) {
    minimal_1.util.Long = long_1;
    minimal_1.configure();
}
function isSet(value) {
    return value !== null && value !== undefined;
}

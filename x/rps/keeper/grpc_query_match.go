package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (k Keeper) MatchAll(c context.Context, req *types.QueryAllMatchRequest) (*types.QueryAllMatchResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var matchs []types.Match
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	matchStore := prefix.NewStore(store, types.KeyPrefix(types.MatchKey))

	pageRes, err := query.Paginate(matchStore, req.Pagination, func(key []byte, value []byte) error {
		var match types.Match
		if err := k.cdc.Unmarshal(value, &match); err != nil {
			return err
		}

		matchs = append(matchs, match)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllMatchResponse{Match: matchs, Pagination: pageRes}, nil
}

func (k Keeper) Match(c context.Context, req *types.QueryGetMatchRequest) (*types.QueryGetMatchResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	ctx := sdk.UnwrapSDKContext(c)
	match, found := k.GetMatch(ctx, req.Id)
	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetMatchResponse{Match: match}, nil
}

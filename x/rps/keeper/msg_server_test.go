package keeper_test

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.RpsKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}

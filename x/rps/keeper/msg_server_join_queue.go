package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func (k msgServer) JoinQueue(goCtx context.Context, msg *types.MsgJoinQueue) (*types.MsgJoinQueueResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	queue, found := k.Keeper.GetQueue(ctx)
	if !found {
		panic("Queue not found")
	}
	if queue.Address == "" {
		queue.Address = msg.Creator
	} else if msg.Creator == queue.Address {
		return nil, types.ErrAlreadyInQueue
	} else {
		var match = types.Match{
			Player1: queue.Address,
			Player2: msg.Creator,
		}
		id := k.AppendMatch(ctx, match)
		for _, addr := range []string{match.Player1, match.Player2} {
			player, found := k.GetPlayer(ctx, addr)
			if !found {
				player = types.Player{
					Index:     addr,
					Ongoing:   []uint64{},
					Completed: []uint64{},
				}
			}
			player.Ongoing = append(player.Ongoing, id)
			k.SetPlayer(ctx, player)
		}
		queue.Address = ""
	}
	k.Keeper.SetQueue(ctx, queue)

	return &types.MsgJoinQueueResponse{}, nil
}

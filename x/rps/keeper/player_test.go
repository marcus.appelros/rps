package keeper_test

import (
	"strconv"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/testutil/nullify"
	"gitlab.com/marcus.appelros/rps/x/rps/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func createNPlayer(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.Player {
	items := make([]types.Player, n)
	for i := range items {
		items[i].Index = strconv.Itoa(i)

		keeper.SetPlayer(ctx, items[i])
	}
	return items
}

func TestPlayerGet(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNPlayer(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetPlayer(ctx,
			item.Index,
		)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&rst),
		)
	}
}
func TestPlayerRemove(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNPlayer(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemovePlayer(ctx,
			item.Index,
		)
		_, found := keeper.GetPlayer(ctx,
			item.Index,
		)
		require.False(t, found)
	}
}

func TestPlayerGetAll(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNPlayer(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllPlayer(ctx)),
	)
}

package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/testutil/nullify"
	"gitlab.com/marcus.appelros/rps/x/rps/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func createNMatch(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.Match {
	items := make([]types.Match, n)
	for i := range items {
		items[i].Id = keeper.AppendMatch(ctx, items[i])
	}
	return items
}

func TestMatchGet(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNMatch(keeper, ctx, 10)
	for _, item := range items {
		got, found := keeper.GetMatch(ctx, item.Id)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&got),
		)
	}
}

func TestMatchRemove(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNMatch(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveMatch(ctx, item.Id)
		_, found := keeper.GetMatch(ctx, item.Id)
		require.False(t, found)
	}
}

func TestMatchGetAll(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNMatch(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllMatch(ctx)),
	)
}

func TestMatchCount(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	items := createNMatch(keeper, ctx, 10)
	count := uint64(len(items))
	require.Equal(t, count, keeper.GetMatchCount(ctx))
}

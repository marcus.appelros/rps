package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/testutil/nullify"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func TestQueueQuery(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	item := createTestQueue(keeper, ctx)
	for _, tc := range []struct {
		desc     string
		request  *types.QueryGetQueueRequest
		response *types.QueryGetQueueResponse
		err      error
	}{
		{
			desc:     "First",
			request:  &types.QueryGetQueueRequest{},
			response: &types.QueryGetQueueResponse{Queue: item},
		},
		{
			desc: "InvalidRequest",
			err:  status.Error(codes.InvalidArgument, "invalid request"),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			response, err := keeper.Queue(wctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
				require.Equal(t,
					nullify.Fill(tc.response),
					nullify.Fill(response),
				)
			}
		})
	}
}

package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"

	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/testutil/nullify"
	"gitlab.com/marcus.appelros/rps/x/rps/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func createTestQueue(keeper *keeper.Keeper, ctx sdk.Context) types.Queue {
	item := types.Queue{}
	keeper.SetQueue(ctx, item)
	return item
}

func TestQueueGet(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	item := createTestQueue(keeper, ctx)
	rst, found := keeper.GetQueue(ctx)
	require.True(t, found)
	require.Equal(t,
		nullify.Fill(&item),
		nullify.Fill(&rst),
	)
}

func TestQueueRemove(t *testing.T) {
	keeper, ctx := keepertest.RpsKeeper(t)
	createTestQueue(keeper, ctx)
	keeper.RemoveQueue(ctx)
	_, found := keeper.GetQueue(ctx)
	require.False(t, found)
}

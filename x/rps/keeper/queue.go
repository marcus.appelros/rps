package keeper

import (
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

// SetQueue set queue in the store
func (k Keeper) SetQueue(ctx sdk.Context, queue types.Queue) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	b := k.cdc.MustMarshal(&queue)
	store.Set([]byte{0}, b)
}

// GetQueue returns queue
func (k Keeper) GetQueue(ctx sdk.Context) (val types.Queue, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))

	b := store.Get([]byte{0})
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveQueue removes queue from the store
func (k Keeper) RemoveQueue(ctx sdk.Context) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.QueueKey))
	store.Delete([]byte{0})
}

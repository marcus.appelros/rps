package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/rps/x/rps/rules"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func (k msgServer) SubmitMove(goCtx context.Context, msg *types.MsgSubmitMove) (*types.MsgSubmitMoveResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	match, found := k.GetMatch(ctx, msg.MatchId)
	if !found {
		return nil, types.ErrMatchNotFound
	}
	if match.Winner > 0 {
		return nil, types.ErrMatchCompleted
	}
	if msg.Creator == match.Player1 {
		match.Player1Move = msg.MoveId
	} else if msg.Creator == match.Player2 {
		match.Player2Move = msg.MoveId
	} else {
		return nil, types.ErrNotInMatch
	}
	if match.Player1Move > 0 && match.Player2Move > 0 {
		for _, addr := range []string{match.Player1, match.Player2} {
			player, found := k.GetPlayer(ctx, addr)
			if !found {
				panic("Player not found even though they finished a match.")
			}
			for i, mid := range player.Ongoing {
				if mid == match.Id {
					player.Ongoing = append(player.Ongoing[:i], player.Ongoing[i+1:]...)
					break
				}
			}
			player.Completed = append(player.Completed, match.Id)
			k.SetPlayer(ctx, player)
		}
		match.Winner = rules.DetermineWinner(match.Player1Move, match.Player2Move)
		player1acc, err := sdk.AccAddressFromBech32(match.Player1)
		if err != nil {
			panic(err)
		}
		player2acc, err := sdk.AccAddressFromBech32(match.Player2)
		if err != nil {
			panic(err)
		}
		if match.Winner == 3 {
			err = k.bankKeeper.MintCoins(ctx, types.ModuleName, sdk.NewCoins(sdk.NewCoin("draw", sdk.NewInt(2))))
			if err != nil {
				panic(err)
			}
			err = k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.ModuleName, player1acc, sdk.NewCoins(sdk.NewCoin("draw", sdk.NewInt(1))))
			if err != nil {
				panic(err)
			}
			err = k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.ModuleName, player2acc, sdk.NewCoins(sdk.NewCoin("draw", sdk.NewInt(1))))
			if err != nil {
				panic(err)
			}
		} else {
			err = k.bankKeeper.MintCoins(ctx, types.ModuleName, sdk.NewCoins(sdk.NewCoin("win", sdk.NewInt(1))))
			if err != nil {
				panic(err)
			}
			if match.Winner == 1 {
				err = k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.ModuleName, player1acc, sdk.NewCoins(sdk.NewCoin("win", sdk.NewInt(1))))
				if err != nil {
					panic(err)
				}
			} else if match.Winner == 2 {
				err = k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.ModuleName, player2acc, sdk.NewCoins(sdk.NewCoin("win", sdk.NewInt(1))))
				if err != nil {
					panic(err)
				}
			}
		}
	}
	k.SetMatch(ctx, match)

	return &types.MsgSubmitMoveResponse{}, nil
}

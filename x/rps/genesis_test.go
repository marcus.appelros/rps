package rps_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	keepertest "gitlab.com/marcus.appelros/rps/testutil/keeper"
	"gitlab.com/marcus.appelros/rps/testutil/nullify"
	"gitlab.com/marcus.appelros/rps/x/rps"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		Params: types.DefaultParams(),

		Queue: &types.Queue{
			Address: "33",
		},
		MatchList: []types.Match{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		MatchCount: 2,
		PlayerList: []types.Player{
			{
				Index: "0",
			},
			{
				Index: "1",
			},
		},
		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.RpsKeeper(t)
	rps.InitGenesis(ctx, *k, genesisState)
	got := rps.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	nullify.Fill(&genesisState)
	nullify.Fill(got)

	require.Equal(t, genesisState.Queue, got.Queue)
	require.ElementsMatch(t, genesisState.MatchList, got.MatchList)
	require.Equal(t, genesisState.MatchCount, got.MatchCount)
	require.ElementsMatch(t, genesisState.PlayerList, got.PlayerList)
	// this line is used by starport scaffolding # genesis/test/assert
}

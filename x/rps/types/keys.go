package types

const (
	// ModuleName defines the module name
	ModuleName = "rps"

	// StoreKey defines the primary module store key
	StoreKey = ModuleName

	// RouterKey is the message route for slashing
	RouterKey = ModuleName

	// QuerierRoute defines the module's query routing key
	QuerierRoute = ModuleName

	// MemStoreKey defines the in-memory store key
	MemStoreKey = "mem_rps"
)

func KeyPrefix(p string) []byte {
	return []byte(p)
}

const (
	QueueKey = "Queue-value-"
)

const (
	MatchKey      = "Match-value-"
	MatchCountKey = "Match-count-"
)

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

const TypeMsgJoinQueue = "join_queue"

var _ sdk.Msg = &MsgJoinQueue{}

func NewMsgJoinQueue(creator string) *MsgJoinQueue {
	return &MsgJoinQueue{
		Creator: creator,
	}
}

func (msg *MsgJoinQueue) Route() string {
	return RouterKey
}

func (msg *MsgJoinQueue) Type() string {
	return TypeMsgJoinQueue
}

func (msg *MsgJoinQueue) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgJoinQueue) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgJoinQueue) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

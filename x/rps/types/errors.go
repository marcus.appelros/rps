package types

// DONTCOVER

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/rps module sentinel errors
var (
	ErrAlreadyInQueue = sdkerrors.Register(ModuleName, 1100, "Address already in queue")
	ErrInvalidMove    = sdkerrors.Register(ModuleName, 1110, "Move not rock (1), paper (2) or scissors (3)")
	ErrMatchNotFound  = sdkerrors.Register(ModuleName, 1120, "Match not found")
	ErrMatchCompleted = sdkerrors.Register(ModuleName, 1130, "Match already completed")
	ErrNotInMatch     = sdkerrors.Register(ModuleName, 1140, "You are not in this match!")
)

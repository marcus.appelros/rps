package types_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

func TestGenesisState_Validate(t *testing.T) {
	for _, tc := range []struct {
		desc     string
		genState *types.GenesisState
		valid    bool
	}{
		{
			desc:     "default is valid",
			genState: types.DefaultGenesis(),
			valid:    true,
		},
		{
			desc: "valid genesis state",
			genState: &types.GenesisState{

				Queue: &types.Queue{
					Address: "66",
				},
				MatchList: []types.Match{
					{
						Id: 0,
					},
					{
						Id: 1,
					},
				},
				MatchCount: 2,
				PlayerList: []types.Player{
					{
						Index: "0",
					},
					{
						Index: "1",
					},
				},
				// this line is used by starport scaffolding # types/genesis/validField
			},
			valid: true,
		},
		{
			desc: "duplicated match",
			genState: &types.GenesisState{
				MatchList: []types.Match{
					{
						Id: 0,
					},
					{
						Id: 0,
					},
				},
			},
			valid: false,
		},
		{
			desc: "invalid match count",
			genState: &types.GenesisState{
				MatchList: []types.Match{
					{
						Id: 1,
					},
				},
				MatchCount: 0,
			},
			valid: false,
		},
		{
			desc: "duplicated player",
			genState: &types.GenesisState{
				PlayerList: []types.Player{
					{
						Index: "0",
					},
					{
						Index: "0",
					},
				},
			},
			valid: false,
		},
		// this line is used by starport scaffolding # types/genesis/testcase
	} {
		t.Run(tc.desc, func(t *testing.T) {
			err := tc.genState.Validate()
			if tc.valid {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
			}
		})
	}
}

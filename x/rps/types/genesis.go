package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		Queue:      &Queue{""},
		MatchList:  []Match{},
		PlayerList: []Player{},
		// this line is used by starport scaffolding # genesis/types/default
		Params: DefaultParams(),
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// Check for duplicated ID in match
	matchIdMap := make(map[uint64]bool)
	matchCount := gs.GetMatchCount()
	for _, elem := range gs.MatchList {
		if _, ok := matchIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for match")
		}
		if elem.Id >= matchCount {
			return fmt.Errorf("match id should be lower or equal than the last id")
		}
		matchIdMap[elem.Id] = true
	}
	// Check for duplicated index in player
	playerIndexMap := make(map[string]struct{})

	for _, elem := range gs.PlayerList {
		index := string(PlayerKey(elem.Index))
		if _, ok := playerIndexMap[index]; ok {
			return fmt.Errorf("duplicated index for player")
		}
		playerIndexMap[index] = struct{}{}
	}
	// this line is used by starport scaffolding # genesis/types/validate

	return gs.Params.Validate()
}

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

const TypeMsgSubmitMove = "submit_move"

var _ sdk.Msg = &MsgSubmitMove{}

func NewMsgSubmitMove(creator string, matchId uint64, moveId uint64) *MsgSubmitMove {
	return &MsgSubmitMove{
		Creator: creator,
		MatchId: matchId,
		MoveId:  moveId,
	}
}

func (msg *MsgSubmitMove) Route() string {
	return RouterKey
}

func (msg *MsgSubmitMove) Type() string {
	return TypeMsgSubmitMove
}

func (msg *MsgSubmitMove) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgSubmitMove) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgSubmitMove) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	if msg.MoveId != 1 && msg.MoveId != 2 && msg.MoveId != 3 {
		return ErrInvalidMove
	}
	return nil
}

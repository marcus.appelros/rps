// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: rps/match.proto

package types

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type Match struct {
	Id          uint64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Player1     string `protobuf:"bytes,2,opt,name=player1,proto3" json:"player1,omitempty"`
	Player2     string `protobuf:"bytes,3,opt,name=player2,proto3" json:"player2,omitempty"`
	Player1Move uint64 `protobuf:"varint,4,opt,name=player1Move,proto3" json:"player1Move,omitempty"`
	Player2Move uint64 `protobuf:"varint,5,opt,name=player2Move,proto3" json:"player2Move,omitempty"`
	Winner      uint64 `protobuf:"varint,6,opt,name=winner,proto3" json:"winner,omitempty"`
}

func (m *Match) Reset()         { *m = Match{} }
func (m *Match) String() string { return proto.CompactTextString(m) }
func (*Match) ProtoMessage()    {}
func (*Match) Descriptor() ([]byte, []int) {
	return fileDescriptor_2beb165675d3ee87, []int{0}
}
func (m *Match) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Match) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Match.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Match) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Match.Merge(m, src)
}
func (m *Match) XXX_Size() int {
	return m.Size()
}
func (m *Match) XXX_DiscardUnknown() {
	xxx_messageInfo_Match.DiscardUnknown(m)
}

var xxx_messageInfo_Match proto.InternalMessageInfo

func (m *Match) GetId() uint64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Match) GetPlayer1() string {
	if m != nil {
		return m.Player1
	}
	return ""
}

func (m *Match) GetPlayer2() string {
	if m != nil {
		return m.Player2
	}
	return ""
}

func (m *Match) GetPlayer1Move() uint64 {
	if m != nil {
		return m.Player1Move
	}
	return 0
}

func (m *Match) GetPlayer2Move() uint64 {
	if m != nil {
		return m.Player2Move
	}
	return 0
}

func (m *Match) GetWinner() uint64 {
	if m != nil {
		return m.Winner
	}
	return 0
}

func init() {
	proto.RegisterType((*Match)(nil), "marcus.appelros.rps.rps.Match")
}

func init() { proto.RegisterFile("rps/match.proto", fileDescriptor_2beb165675d3ee87) }

var fileDescriptor_2beb165675d3ee87 = []byte{
	// 215 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2f, 0x2a, 0x28, 0xd6,
	0xcf, 0x4d, 0x2c, 0x49, 0xce, 0xd0, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x12, 0xcf, 0x4d, 0x2c,
	0x4a, 0x2e, 0x2d, 0xd6, 0x4b, 0x2c, 0x28, 0x48, 0xcd, 0x29, 0xca, 0x2f, 0xd6, 0x2b, 0x2a, 0x00,
	0x63, 0xa5, 0xe5, 0x8c, 0x5c, 0xac, 0xbe, 0x20, 0x85, 0x42, 0x7c, 0x5c, 0x4c, 0x99, 0x29, 0x12,
	0x8c, 0x0a, 0x8c, 0x1a, 0x2c, 0x41, 0x4c, 0x99, 0x29, 0x42, 0x12, 0x5c, 0xec, 0x05, 0x39, 0x89,
	0x95, 0xa9, 0x45, 0x86, 0x12, 0x4c, 0x0a, 0x8c, 0x1a, 0x9c, 0x41, 0x30, 0x2e, 0x42, 0xc6, 0x48,
	0x82, 0x19, 0x59, 0xc6, 0x48, 0x48, 0x81, 0x8b, 0x1b, 0xaa, 0xc8, 0x37, 0xbf, 0x2c, 0x55, 0x82,
	0x05, 0x6c, 0x18, 0xb2, 0x10, 0x42, 0x85, 0x11, 0x58, 0x05, 0x2b, 0xb2, 0x0a, 0xb0, 0x90, 0x90,
	0x18, 0x17, 0x5b, 0x79, 0x66, 0x5e, 0x5e, 0x6a, 0x91, 0x04, 0x1b, 0x58, 0x12, 0xca, 0x73, 0x72,
	0x39, 0xf1, 0x48, 0x8e, 0xf1, 0xc2, 0x23, 0x39, 0xc6, 0x07, 0x8f, 0xe4, 0x18, 0x27, 0x3c, 0x96,
	0x63, 0xb8, 0xf0, 0x58, 0x8e, 0xe1, 0xc6, 0x63, 0x39, 0x86, 0x28, 0xad, 0xf4, 0xcc, 0x92, 0x9c,
	0xc4, 0x24, 0xbd, 0xe4, 0xfc, 0x5c, 0x7d, 0x34, 0x7f, 0xea, 0x83, 0x02, 0xa2, 0x02, 0x4c, 0x96,
	0x54, 0x16, 0xa4, 0x16, 0x27, 0xb1, 0x81, 0xc3, 0xc3, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x2c,
	0x17, 0xb2, 0x85, 0x22, 0x01, 0x00, 0x00,
}

func (m *Match) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Match) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *Match) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.Winner != 0 {
		i = encodeVarintMatch(dAtA, i, uint64(m.Winner))
		i--
		dAtA[i] = 0x30
	}
	if m.Player2Move != 0 {
		i = encodeVarintMatch(dAtA, i, uint64(m.Player2Move))
		i--
		dAtA[i] = 0x28
	}
	if m.Player1Move != 0 {
		i = encodeVarintMatch(dAtA, i, uint64(m.Player1Move))
		i--
		dAtA[i] = 0x20
	}
	if len(m.Player2) > 0 {
		i -= len(m.Player2)
		copy(dAtA[i:], m.Player2)
		i = encodeVarintMatch(dAtA, i, uint64(len(m.Player2)))
		i--
		dAtA[i] = 0x1a
	}
	if len(m.Player1) > 0 {
		i -= len(m.Player1)
		copy(dAtA[i:], m.Player1)
		i = encodeVarintMatch(dAtA, i, uint64(len(m.Player1)))
		i--
		dAtA[i] = 0x12
	}
	if m.Id != 0 {
		i = encodeVarintMatch(dAtA, i, uint64(m.Id))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func encodeVarintMatch(dAtA []byte, offset int, v uint64) int {
	offset -= sovMatch(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *Match) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Id != 0 {
		n += 1 + sovMatch(uint64(m.Id))
	}
	l = len(m.Player1)
	if l > 0 {
		n += 1 + l + sovMatch(uint64(l))
	}
	l = len(m.Player2)
	if l > 0 {
		n += 1 + l + sovMatch(uint64(l))
	}
	if m.Player1Move != 0 {
		n += 1 + sovMatch(uint64(m.Player1Move))
	}
	if m.Player2Move != 0 {
		n += 1 + sovMatch(uint64(m.Player2Move))
	}
	if m.Winner != 0 {
		n += 1 + sovMatch(uint64(m.Winner))
	}
	return n
}

func sovMatch(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozMatch(x uint64) (n int) {
	return sovMatch(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Match) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowMatch
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Match: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Match: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Id", wireType)
			}
			m.Id = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Id |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Player1", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthMatch
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthMatch
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Player1 = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Player2", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthMatch
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthMatch
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Player2 = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 4:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Player1Move", wireType)
			}
			m.Player1Move = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Player1Move |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 5:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Player2Move", wireType)
			}
			m.Player2Move = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Player2Move |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 6:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Winner", wireType)
			}
			m.Winner = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Winner |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipMatch(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthMatch
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipMatch(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowMatch
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowMatch
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthMatch
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupMatch
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthMatch
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthMatch        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowMatch          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupMatch = fmt.Errorf("proto: unexpected end of group")
)

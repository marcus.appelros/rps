package rules

func DetermineWinner(player1move, player2move uint64) uint64 {
	if player1move == 0 || player2move == 0 {
		return 0
	}
	if player1move == player2move {
		return 3
	}
	if player1move == player2move+1 || player1move == player2move-2 {
		return 1
	}
	if player2move == player1move+1 || player2move == player1move-2 {
		return 2
	}
	return 4
}

package rps

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/marcus.appelros/rps/x/rps/keeper"
	"gitlab.com/marcus.appelros/rps/x/rps/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// Set if defined
	if genState.Queue != nil {
		k.SetQueue(ctx, *genState.Queue)
	}
	// Set all the match
	for _, elem := range genState.MatchList {
		k.SetMatch(ctx, elem)
	}

	// Set match count
	k.SetMatchCount(ctx, genState.MatchCount)
	// Set all the player
	for _, elem := range genState.PlayerList {
		k.SetPlayer(ctx, elem)
	}
	// this line is used by starport scaffolding # genesis/module/init
	k.SetParams(ctx, genState.Params)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()
	genesis.Params = k.GetParams(ctx)

	// Get all queue
	queue, found := k.GetQueue(ctx)
	if found {
		genesis.Queue = &queue
	}
	genesis.MatchList = k.GetAllMatch(ctx)
	genesis.MatchCount = k.GetMatchCount(ctx)
	genesis.PlayerList = k.GetAllPlayer(ctx)
	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}
